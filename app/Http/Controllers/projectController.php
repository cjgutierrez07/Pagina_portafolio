<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\project;
use App\Models\state;

class projectController extends Controller
{
    //
      public function index()
	{
		
		try{
           $proyectos = project::all();
       
        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('projects','Error'.$e->getmessage());
        }   
           

//dd($todos);
        return \View::make('projects',compact('proyectos'));
	}

    public function edit($id)
    {

            $Proyectos= project::find($id);
            $Estados = state::select('id','nombre')->get();
    
        return \View::make('editProject', compact('Proyectos','Estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
            
    
        
            //$id = $request->id;
            $Proyecto = project::find($request->id);
            $Proyecto->image=$request->image;
            $Proyecto->name=$request->name;
            $Proyecto->description=$request->description;
            $Proyecto->estado_id=$request->state_id;
            $Proyecto->save();
            
        return redirect()->action('projectController@index');

    }
          public function nuevo()
  {
    
    $Estados = state::select('id','nombre')->get();
    return \View::make('crearProject', compact('Estados'));
  }
    public function store(Request $request)
  {
    try
    {
      $project= new project;
      $project->create($request->all());
    }
    catch(\Exception $e)
        {
      dd($e);
            Session::flash('message','Error'.$e->getmessage());     
        }   
    return redirect('/projects');
  }
}
