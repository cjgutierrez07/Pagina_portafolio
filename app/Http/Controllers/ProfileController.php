<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use  App\User;
use App\Models\rol;
use App\Models\pais;
use Storage;
class ProfileController extends Controller
{
    //
	private $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

    public function index()
	{
		
		try{
			$id= $this->auth->user()->id;
           $Usuario = User::find($id);
       
        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('message','Error'.$e->getmessage());
        }   
           
        //dd($Usuario);

        return \View::make('profile',compact('Usuario'));
	}
	  public function edit()
    {

           $id= $this->auth->user()->id;
           $Usuario = User::find($id);
          	$Paises = pais::select('id','nombre')->get();
            $Roles = rol::select('id','nombre')->get();
    
        return \View::make('editProfile', compact('Usuario','Paises','Roles'));
    }

   	public function update(Request $request)
	{
			

		
			//$id = $request->id;
		$img=$request->file('img');
		if ($img != null)
			{
			$file_route = time().'_'.$img->getClientOriginalName();
			Storage::disk('profile_pictures')->put($file_route, file_get_contents($img->getRealPath()));
		}
			$pefil = User::find($request->id);
			$pefil->name=$request->name;
			$pefil->last_name=$request->last_name;

			if ($img != null)
			{
			$pefil->img=$file_route;
}
			

			$pefil->email=$request->email;
			$pefil->pais_id=$request->pais_id;
			$pefil->rol_id=$request->rol_id;
		
			$pefil->save();

			

			return redirect('/profile');
	
	}

	
}
