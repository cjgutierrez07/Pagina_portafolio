<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\service;
use App\Models\state;

class ServiceController extends Controller
{
    //
        public function index()
	{
		
		try{
           $Servicios = service::all();
       
        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('message','Error'.$e->getmessage());
        }   
           

//dd($todos);
        return \View::make('services',compact('Servicios'));
	}

	public function edit($id)
	{

			$Servicios= service::find($id);
			$Estados = state::select('id','nombre')->get();
	
		return \View::make('editService', compact('Servicios','Estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
			
	
		
			//$id = $request->id;
			$Servicioz = service::find($request->id);
			$Servicioz->image=$request->image;
			$Servicioz->name=$request->name;
			$Servicioz->description=$request->description;
			$Servicioz->estado_id=$request->state_id;
			$Servicioz->save();
			
		return redirect()->action('ServiceController@index');

	}


			public function nuevo()
	{
 		
 		$Estados = state::select('id','nombre')->get();
		return \View::make('crearService', compact('Estados'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try
		{
			$service= new service;
			$service->create($request->all());
		}
		catch(\Exception $e)
        {
 			dd($e);
            Session::flash('message','Error'.$e->getmessage());     
        }  	
		return redirect('/services');
	}

}
