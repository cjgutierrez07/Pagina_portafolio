<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\imagen;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->rol_id==2){

            return view('welcome');
        }
        else{
            return view('home');
        }     
    }
        public function imagen(Request $request)
    {
            

        
            //$id = $request->id;
            $img1=$request->file('img1');
            if ($img1 != null)
            {
                  $file_route1 = time().'_'.$img1->getClientOriginalName();
            Storage::disk('profile_pictures')->put($file_route1, file_get_contents($img1->getRealPath()));
            }
          

             $img2=$request->file('img2');
             if ($img2 != null)
             {
            $file_route2 = time().'_'.$img2->getClientOriginalName();
            Storage::disk('profile_pictures')->put($file_route2, file_get_contents($img2->getRealPath()));
    }

             $img3=$request->file('img3');
             if ($img3 != null)
             {
            $file_route3 = time().'_'.$img3->getClientOriginalName();
            Storage::disk('profile_pictures')->put($file_route3, file_get_contents($img3->getRealPath()));
// dd($img3);
        }
        if ($img3 != null || $img2 != null || $img1 != null)
        {
            $pefil = imagen::find(1);
  

            $pefil->image1=$file_route1;

              $pefil->image2=$file_route2;
                $pefil->image3=$file_route3;


        
            $pefil->save();
        }

            

            return redirect('/imagenes');
    
    }

}
