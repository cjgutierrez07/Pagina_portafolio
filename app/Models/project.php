<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    //
     protected $table = 'projects';
      protected $fillable = ['image', 'name','description','estado_id'];
    protected $guarded = ['id'];
}
