<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
     protected $table = 'contact';
    protected $fillable = ['name', 'email', 'subject','message','estado_id'];
 	protected $guarded = ['id'];
}
