<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class imagen extends Model
{
    //
    protected $table = 'imagenes';
     protected $fillable = ['image1','image2','image3'];
    protected $guarded = ['id'];

}
