<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                   Schema::table('users', function($table)
        {
            $table->foreign('pais_id')
                ->references('id')
                ->on('paises')
                ->onUpdate('cascade');
        });

                              Schema::table('contact', function($table)
        {
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onUpdate('cascade');
        });
                              Schema::table('users', function($table)
        {
            $table->foreign('rol_id')
                ->references('id')
                ->on('roles')
                ->onUpdate('cascade');
        });

                               Schema::table('services', function($table)
        {
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onUpdate('cascade');
        });
                                Schema::table('projects', function($table)
        {
            $table->foreign('estado_id')
                ->references('id')
                ->on('estados')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
