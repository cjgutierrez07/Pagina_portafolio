@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
          <div class="col-md-11 col-md-offset-1">
                
  @foreach($Contactos as $contact)
  
          <div class="panel ">
            <div class="panel-heading">de: {{$contact->name}} °°° {{$contact->email}}<br>
                {{$contact->created_at->diffForHumans()}}</div>
            <div class="panel panel-footer">Asunto: {{$contact->subject}}</div>
            <div class="panel-body" style="word-wrap: break-word;">Mensaje: {{$contact->message}} </div>
            <div class="panel-footer">{{$contact->email}}
             <br> 
             {{$contact->created_at->diffForHumans()}}
             <div> <a class="btn btn-perro pull-right" href="{{url('message',['id'=> $contact->id])}}">Responder </a>
             </div>
         </div>
        </div>


    @endforeach

        </div>
    </div>
</div>
@endsection