@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
       <div class="col-md-11 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Projects <a  href="{{url('project/add')}}" class="btn btn-xs btn-success">Nuevo</a></div>

                <div class="panel-body">
                       <div class="text-center">
        

        <div class="row">

             @foreach($proyectos as $proyecto)
            <div class="col-md-6 well jumbotron">
                <div class="wow fadeInLeft" data-wow-delay="0.2s">
                <div class="service-box">
                    <div class="service-icon">
                        <span class="{{$proyecto->image}}"></span> 
                    </div>
                    <div class="service-desc" style="word-wrap: break-word;">                      
                        <h5>{{$proyecto->name}}</h5>
                        <p>
                             {{str_limit( $proyecto->description,25)}}
                        </p>
                
                    </div>
                    <a href="{{ route('project',['id' => $proyecto->id] )}}" class="btn btn-perro">Editar</a>
                </div>
                </div>
            </div>
           
            @endforeach
           
        </div>      
  
        </div>
                    
                </div>
           
        </div>
    </div>
</div>
@endsection



  