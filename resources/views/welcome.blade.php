<?php
 use App\Models\service;
 use App\Models\project;
  use App\Models\imagen;
 
        $servicios = service::all()
        ->where('estado_id','=','1');
       
         $proyectos = project::all()
        ->where('estado_id','=','1');
// dd($servicios);
       
  $imagens = imagen::all();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Christhian Gutierrez</title>
    
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/nivo-lightbox.css" rel="stylesheet" />
    <link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="css/owl.theme.css" rel="stylesheet" media="screen" />
    <link href="css/flexslider.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="color/default.css" rel="stylesheet">


</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
    
    <section id="intro" class="home-slide text-light">

        <!-- Carousel -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                @foreach($imagens as $img)
                <div class="item active">
                    <img src="profile_pictures/{{$img->image1}}" alt="First slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                <span>CHRISTHIAN GUTIERREZ</span>
                            </h2>
                            <br>
                            <h3>
                                 <span>Tecnico porgramacion e software aprendiz tecnologo adsi.</span>
                            </h3>
                            <br>
                            <div class="">
                                </div>
                        </div>
                    </div><!-- /header-text -->
                </div>
                <div class="item">
                    <img src="profile_pictures/{{$img->image2}}" alt="Second slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                               <span>CHRISTHIAN GUTIERREZ</span>
                            </h2>
                            <br>
                            <h3>
                                <span>Tecnico porgramacion e software aprendiz tecnologo adsi.</span>
                            </h3>
                            <br>
                            <div class="">
                                 </div>
                        </div>
                    </div><!-- /header-text -->
                </div>
                <div class="item">
                    <img src="profile_pictures/{{$img->image3}}" alt="Third slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                 <span>CHRISTHIAN GUTIERREZ</span>
                            </h2>
                            <br>
                            <h3>
                                <span>Tecnico porgramacion e software aprendiz tecnologo adsi.</span>
                            </h3>
                            <br>
                            <div class="">
                               </div>
                        </div>
                    </div><!-- /header-text -->
                </div>

                @endforeach
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div><!-- /carousel -->

    </section>
    <!-- /Section: intro -->
    
    
    <!-- Navigation -->
    <div id="navigation">
        <nav class="navbar navbar-custom" role="navigation">
                              <div class="container">
                                    <div class="row">
                                          <div class="col-md-5">
                                                   <div class="site-logo">
                                                            <a href="" class="brand">CHRISTHIAN GUTIERREZ</a>
                                                    </div>
                                          </div>
                                          

                                          <div class="col-md-7">
                         
                                                      <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
                                          </div>
                                                      <!-- Collect the nav links, forms, and other content for toggling -->
                                                      <div class="collapse navbar-collapse" id="menu">
                                                            <ul class="nav navbar-nav navbar-right">
                                                                  <li class="active"><a href="#intro">Home</a></li>
                                                                  <li><a href="#about">About</a></li>
                                                                   <li><a href="#service">Services</a></li>
                                                                  <li><a href="#contact">Contact</a></li>
                                                                      @if (Route::has('login'))
                                                          
                                                                        @if (Auth::check() &&  Auth::user()->rol_id == 1)
                                                                            <li>  <a href="{{ url('/home') }}">Menu</a></li>
                                                                             @elseif (Auth::check() &&  Auth::user()->rol_id == 2)
                                                                             <li>  <a href="{{ url('/logout') }}">Salir</a></li>
                                                                        @else
                                                                            <li> <a href="#intro" class="" data-toggle="modal" data-target="#myModal">Login</a></li>
                                                                        @endif

                                                                              
                                                                               
                                                                @endif 
                                                            </ul>

                                                        
                                                      </div>
                                                      <!-- /.Navbar-collapse -->
                          
                                          </div>
                                    </div>
                              </div>
                              <!-- /.container -->
                        </nav>
    </div> 
    <!-- /Navigation -->  

    <!-- Section: about -->
    <section id="about" class="home-section color-dark bg-white">
        <div class="container marginbot-50">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
                    <div class="section-heading text-center">
                    <h2 class="h-bold">Projects</h2>
                    <div class="divider-header"></div>
                    <p>Proyectos desarrollados</p>
                    </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="container">

        
        <div class="row">
        
         
          @foreach($proyectos as $proyecto)
            <div class="col-md-3">
                <div class="wow fadeInLeft" data-wow-delay="0.2s">
                <div class="service-box">
                    <div class="service-icon">
                         <span class="{{$proyecto->image}}"></span> 
                    </div>
                    <div class="service-desc" style="word-wrap: break-word;">                      
                        <h5>{{$proyecto->name}}</h5>
                        <p>
                       {{nl2br( $proyecto->description)}}
                        </p>
                
                    </div>
                </div>
                </div>
            </div>
           
            @endforeach
        

        </div>      
        </div>

    </section>
    <!-- /Section: about -->
    
    
    <!-- Section: services -->
    <section id="service" class="home-section2 color-dark bg-gray">
        <div class="container marginbot-50">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
                    <div class="section-heading text-center">
                    <h2 class="h-bold">Services</h2>
                    <div class="divider-header"></div>
                   
                    </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="text-center">
        

        <div class="row">

             @foreach($servicios as $servicio)
            <div class="col-md-3">
                <div class="wow fadeInLeft" data-wow-delay="0.2s">
                <div class="service-box">
                    <div class="service-icon">
                         <span class="{{$servicio->image}}"></span> 
                    </div>
                    <div class="service-desc" style="word-wrap: break-word;">     
                                   
                        <h5>{{$servicio->name}}</h5>
                      {!!$servicio->description!!}
                    
                                             
                
                    </div>
                </div>
                </div>
            </div>
           
            @endforeach
           
        </div>      
  
        </div>
    </section>
    <!-- /Section: services -->
    

    <!-- Section: contact -->
  
      @include('contact')

<!-- Modal  login-->
@include('login')


<!-- Modal  register-->


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    
                    <div class="text-center">
                        <a href="#intro" class="totop"><i class="fa fa-angle-up fa-3x"></i></a>
                        <p>&copy; Shuffle Theme - All Rights Reserved</p>
                        <div class="credits">
                  
                            <a href="">Christhian Gutierrez</a>  <a href="">1353731</a>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </footer>

    <!-- Core JavaScript Files -->
    <script src="js/jquery.min.js"></script>     
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.easing.min.js"></script> 
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/nivo-lightbox.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="contactform/contactform.js"></script>
    
</body>

</html>

  
