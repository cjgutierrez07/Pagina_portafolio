@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
 <div class="col-md-11 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                     <form class="form-horizontal" role="form" method="POST" action="{{ url('/project/update') }}">
                        {{ csrf_field() }}
                         <input id="id" type="hidden" class="form-control" name="id" value="{{$Proyectos->id}}" required>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Imagen: </label>

                            <div class="col-md-6">
                                <input id="image" type="text" class="form-control" name="image" value="{{$Proyectos->image}}" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    <img src="">
         
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre: </label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$Proyectos->name}}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                  
                   
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descripcion: </label>

                            <div class="col-md-6">

                                <textarea id="description" type="text" class="form-control" name="description"  required>{{$Proyectos->description}}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                    <div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
                         <label for="state_id" class="col-md-4 control-label">Estado: </label>
                    <div class="col-md-6">
                        
                    <select name="state_id"  class="form-control">
                        @foreach($Estados as $pais)

                                                @if($Proyectos->estado_id == $pais->id) 
                                                <option selected value="{{$pais->id}}">{{$pais->nombre}}</option>
    
                                             @else
                                            <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                                 @endif
                                                 @endforeach
                                          
                        
                    </select>
                </div>
                     @if ($errors->has('state_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state_id') }}</strong>
                                    </span>
                                @endif
                    </div>
                   <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-skin">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




