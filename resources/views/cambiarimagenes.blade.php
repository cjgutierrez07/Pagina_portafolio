<?php

 use App\Models\imagen;
  
  $imagens = imagen::all();
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <form method="post" action="{{ url('/guardarImagenes') }}"  enctype="multipart/form-data">  
        {{ csrf_field() }}
 <div class="col-md-11 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Imagenes</div>

                <div class="panel-body ">
            
		
		@foreach ($imagens as $img)
                    <div class="row col-md-10 col-md-offset-1" >
          
                	<img class="img-responsive" width="" src="profile_pictures/{{$img->image1}}">
                	<br>
                	<input type="file" name="img1" class="btn btn-perro">
                </div>

                  <div class="row col-md-10 col-md-offset-1" >
          
                    <img class="img-responsive" width="" src="profile_pictures/{{$img->image2}}">
                    <br>
                    <input type="file" name="img2" class="btn btn-perro">
                </div>

                  <div class="row col-md-10 col-md-offset-1" >
          
                    <img class="img-responsive" width="" src="profile_pictures/{{$img->image3}}">
                    <br>
                    <input type="file" name="img3" class="btn btn-perro">
                </div>
       @endforeach
                <br>
                <div class="row col-md-10">
                    <input type="submit" name="Guardar" class="btn btn-perro" value="Guardar">
               
                  </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection




