
<?php
 use App\Models\Contact;
 use App\Models\imagen;
 try{
        $Contacts = Contact::Select('*')
        ->where('estado_id','=','1')
        ->paginate(5);
        $todos = Contact::all()
        ->where('estado_id','=','1');
        }

        catch(\Exception $e)
        {
            dd($e);
            Session::flash('message','Error'.$e->getmessage());
        }   

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>CHRISTHIAN GUTIERREZ</title>

    <!-- Bootstrap CSS -->    
    <link href="../css2/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../css2/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../css2/elegant-icons-style.css" rel="stylesheet" />
    <link href="../css2/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="../assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">
    <link href="../css2/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="../css2/fullcalendar.css">
    <link href="../css2/widgets.css" rel="stylesheet">
    <link href="../css2/style.css" rel="stylesheet">
    <link href="../css2/style-responsive.css" rel="stylesheet" />
    <link href="../css2/xcharts.min.css" rel=" stylesheet"> 
    <link href="../css2/jquery-ui-1.10.4.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="plugins/fullcalendar/fullcalendar.min.css">


     {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}<!-- Font Awesome -->
     {!! Html::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') !!}<!-- Ionicons -->
     {!! Html::style('dist/css/AdminLTE.min.css') !!}<!-- Theme style -->
     {!! Html::style('dist/css/skins/_all-skins.min.css') !!}    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
     {!! Html::style('plugins/iCheck/flat/blue.css') !!}<!-- iCheck -->
     {!! Html::style('plugins/iCheck/all.css') !!}<!-- iCheck for checkboxes and radio inputs -->
     {!! Html::style('css/academicManage.css') !!}<!-- Personalizados-->
     {!! Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}<!-- bootstrap wysihtml5 - text editor -->
     {!! Html::style('plugins/daterangepicker/daterangepicker-bs3.css') !!}<!-- Daterange picker -->
     {!! Html::style('plugins/datepicker/datepicker3.css') !!}<!-- Date Picker -->
     {!! Html::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}<!-- jvectormap -->
     {!! Html::style('plugins/morris/morris.css') !!}<!-- Morris chart -->
     {!! Html::style('css/academicManage.css') !!}
     {!! Html::style('plugins/fullcalendar/fullcalendar.min.css') !!}
     {!! Html::style('css/StiloMensajesEliminacion.css') !!}
      <script src="ckeditor/ckeditor.js"></script>

  
    <!-- =======================================================
        Theme Name: NiceAdmin
        Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>

  <body >
  <!-- container section start -->

  <section id="container" >
      <div class="particles-js"></div>
      <div class="col-md-12 col-lg-12 arrow">
      <header class="header dark-bg ">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="/" class="logo">CHRISTHIAN GUTIERREZ</a>
            <!--logo end-->

       

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
          
                    <!-- inbox notificatoin start-->
                    <li id="mail_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon-envelope-l"></i>
                            <span class="badge bg-important">{{$todos->count()}}</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-perro"></div>
                            <li>
                                <p class="perro">You have {{$todos->count()}} new messages</p>
                            </li>
                             
                                 @foreach($Contacts as $contact)
                             <li>

                                <a href="{{url('message',['id'=> $contact->id])}}">
                                 
                                    <span class="photo"><img alt="avatar" src="./img/avatar-mini4.jpg"></span>
                                    <span class="subject">
                                    <span class="from"> {{$contact->name}}</span>
                                    <span class="time">{{$contact->created_at->diffForHumans()}}</span>
                                    </span>
                                    <span class="message">
                                           {{str_limit($contact->message,25)}}
                                    </span>
                                </a>
                            </li>
                                            @endforeach

                         
                            <li>
                                <a href="{{url('message')}}">See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox notificatoin end -->
                    <!-- alert notification start-->
                    <li id="alert_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                            <i class="icon-bell-l"></i>
                            <span class="badge bg-important"> 0</span>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <div class="notify-arrow notify-arrow-perro"></div>
                            <li>
                                <p class="perro">
                                  You have 0new notifications</p>
                            </li>
                          
                                                  
                            <li>
                                <a href="#">See all notifications</a>
                            </li>
                        </ul> 
                    </li>
                    <!-- alert notification end-->
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img width="50px" height="50px" src="profile_pictures/{{ \Auth::user()->img }}" class="img-circle">
                            </span>
                            <span class="username">{{ \Auth::user()->name }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="{{url ('/profile')}}"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href=""><i class="icon_mail_alt"></i> My Inbox</a>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"><i class=" llave icon_key_alt"></i> Log Out</a>
                            </li>

                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>    
      </div>  
      <!--header end-->

      <!--sidebar start-->
      <div>
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li >
                      <a href="/home">
                          <i class="color icon_house_alt"></i>
                          <span class="perrogato">Dashboard</span>
                      </a>
                  </li>
                  <li>
                      <a class="" href="{{url('message')}}">
                          <i class="color icon_mail_alt"></i>
                           <span class="perrogato">Mensajes</span>
                      </a>
                  </li>
                  <li>                     
                      <a class="" href="{{url('services')}}">
                          <i class="color icon_piechart"></i>
                          <span class="perrogato">Servicios</span>
                          
                      </a>
                                         
                  </li>
                             
                  <li>                     
                      <a class="" href="{{url('projects')}}">
                          <i class="color icon_easel"></i>
                          <span class="perrogato">Proyectos</span>
                          
                      </a>
                                         
                  </li>
                  
                   <li>                     
                      <a class="" href="{{url('imagenes')}}">
                          <i class="color icon_easel"></i>
                          <span class="perrogato">Imagenes</span>
                          
                      </a>
                                         
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      </div>

      <div class=" arrow" style="margin-top : 5%; margin-left: 5%">@yield('content')</div>
     
  </section>
   
  <!-- container section start -->

    <!-- javascripts -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/fullcalendar/fullcalendar.min.js"></script>>
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery-ui-1.10.4.min.js"></script>
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../js/jquery.scrollTo.min.js"></script>
    <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- charts scripts -->
    <script src="../assets/jquery-knob/js/jquery.knob.js"></script>
    <script src="../js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../js/owl.carousel.js" ></script>
    <!-- jQuery full calendar -->
    <script src="../js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
    <script src="../assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="../js/calendar-custom.js"></script>
    <script src="../js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="../js/jquery.customSelect.min.js" ></script>
    <script src="../assets/chart-master/Chart.js"></script>
   
    <!--custome script for all page-->
    <script src="../js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="../js/sparkline-chart.js"></script>
    <script src="../js/easy-pie-chart.js"></script>
    <script src="../js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="../js/xcharts.min.js"></script>
    <script src="../js/jquery.autosize.min.js"></script>
    <script src="../js/jquery.placeholder.min.js"></script>
    <script src="../js/gdp-data.js"></script>  
    <script src="../js/morris.min.js"></script>
    <script src="../js/sparklines.js"></script>    
    <script src="../js/charts.js"></script>
    <script src="../js/jquery.slimscroll.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
      
      /* ---------- Map ---------- */
    $(function(){
      $('#map').vectorMap({
        map: 'world_mill_en',
        series: {
          regions: [{
            values: gdpData,
            scale: ['#000', '#000'],
            normalizeFunction: 'polynomial'
          }]
        },
        backgroundColor: '#eef3f7',
        onLabelShow: function(e, el, code){
          el.html(el.html()+' (GDP - '+gdpData[code]+')');
        }
      });
    });

  </script>

  </body>
</html>
